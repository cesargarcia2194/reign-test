import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private http: HttpClient) {}

  deleteArticle(id: string): Observable<any>{
    return this.http.delete(`${environment.urlBE}article/${id}`)
  }
  getArticle(): Observable<any>{
    return this.http.get(`${environment.urlBE}article`)
  }
}
