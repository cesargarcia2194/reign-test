import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './http-service.service';
import {Article} from './article.interface';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'client-app';
  prueba = '2020-12-30T00:18:11.000Z | date:shortTime'
  articles:Article[] =[];
  ngOnInit(){
    this.getArticles();
  }
  constructor(private _service: HttpServiceService){

  }

  delete(id: string){
    this._service.deleteArticle(id).subscribe(res => {
      console.log(res)
      this.getArticles();
    })

  }
  getArticles(){
    this._service.getArticle().subscribe(res => {
      this.articles = res;
    });
  }

}
