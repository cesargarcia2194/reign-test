export interface Article {
  author: string;
  createdAt: string;
  objectId: string;
  storyTitle: string;
  storyUrl: string;
  title: string;
  url: string;
  __v: number;
  _id: string;
}
