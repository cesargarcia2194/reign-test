import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { DatabaseModule } from './database/database.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/confg.keys';
import { ArticleModule } from './api/article/article.module';

@Module({
    imports: [
        ConfigModule,
        DatabaseModule, 
        ArticleModule,
        ScheduleModule.forRoot()
    ],
    controllers: [AppController],
    providers: [AppService, ConfigService],
})
export class AppModule {
	static port: number | string

    constructor(private readonly _configService: ConfigService) {
		AppModule.port = this._configService.get(Configuration.PORT)
    }
}
