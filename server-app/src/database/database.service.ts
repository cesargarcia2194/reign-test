import 'dotenv'
import {MongooseModule} from '@nestjs/mongoose';

export const databaseProviders = 
[MongooseModule.forRoot('mongodb://localhost/mongo')]