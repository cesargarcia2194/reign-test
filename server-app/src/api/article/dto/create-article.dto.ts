export class CreateArticleDto {
    title: string;
    storyTitle: string;
    author: string;
    createdAt: Date;
    objectId: string;
    url: string;
    storyUrl: string
}