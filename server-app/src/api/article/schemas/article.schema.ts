import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument =  Article & Document

@Schema()
export class Article {

    @Prop()
    createdAt: Date;
    @Prop()
    title: string;
    @Prop()
    url: string;
    @Prop()
    author: string;
    @Prop()
    storyTitle: string;
    @Prop()
    storyUrl: string;
    @Prop()
    objectId: string;

}
export const ArticleSchema = SchemaFactory.createForClass(Article);