import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDeletedDocument =  ArticleDeleted & Document

@Schema()
export class ArticleDeleted {

    @Prop()
    createdAt: Date;
    @Prop()
    title: string;
    @Prop()
    url: string;
    @Prop()
    author: string;
    @Prop()
    storyTitle: string;
    @Prop()
    storyUrl: string;
    @Prop()
    objectId: string;

}
export const ArticleDeletedSchema = SchemaFactory.createForClass(ArticleDeleted);