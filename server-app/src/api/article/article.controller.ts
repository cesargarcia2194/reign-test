import { Controller, Get, Post, Body, Delete, Param, ParseIntPipe, } from '@nestjs/common';
import { ArticleService } from './article.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article } from './schemas/article.schema';
import { ArticleInterface } from './interfaces/article.interface';

@Controller('article')
export class ArticleController {
    constructor(private readonly _articleService: ArticleService){}

    @Get()
    async getHello(): Promise<Article[]>{
        //this._articleService.getArticlesApi();
        return this._articleService.findAll();
    }

    @Post()
    async createArticle(@Body() createArticleDto:CreateArticleDto ): Promise<Article>{
        return this._articleService.createArticle(createArticleDto);
    }
    @Delete(':id')
    async delete(@Param('id') id: string): Promise<Article>{
        return await this._articleService.delete(id)
    }

    /*@Get('api')
    getArticleApi(): Promise<ArticleInterface[]>{
        const articles = 
        return this._articleService.getArticlesApi();
    }*/

}
