import { Module, HttpModule } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Article, ArticleSchema } from './schemas/article.schema';
import { ArticleDeleted, ArticleDeletedSchema } from './schemas/article-deleted.schema';
@Module({
  imports: [
    MongooseModule.forFeature([
      {name: Article.name, schema: ArticleSchema},
      {name: ArticleDeleted.name, schema: ArticleDeletedSchema}]),
    HttpModule
  ],
  providers: [ArticleService],
  controllers: [ArticleController]
})
export class ArticleModule {}
