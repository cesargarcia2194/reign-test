import { Injectable, HttpService, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Article, ArticleDocument } from './schemas/article.schema';
import { Model } from 'mongoose';
import { CreateArticleDto } from './dto/create-article.dto';
import { ArticleInterface } from './interfaces/article.interface';
import { ArticleDeleted, ArticleDeletedDocument } from './schemas/article-deleted.schema';

@Injectable()
export class ArticleService {

    constructor(
        @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
        @InjectModel(ArticleDeleted.name) private articleDeletedModel: Model<ArticleDeletedDocument>,
        private httpService: HttpService){}
    
    async findAll(): Promise<Article[]>{
        return this.articleModel
            .find()
            .sort({createdAt: -1})
            .exec();
    }

    async createArticle(createArticleDto: CreateArticleDto): Promise<Article>{
        const createdArticle = new this.articleModel(createArticleDto);
        console.log(createdArticle)
        return createdArticle.save();      
    }
    
    async delete(id: string): Promise<Article>{
        const articleExist = await this.articleModel.findByIdAndDelete(id);
        if(!articleExist){
            throw new NotFoundException()
        }
        const articleEliminated = {...articleExist['_doc']}
        delete articleEliminated._id
        await new this.articleDeletedModel(articleEliminated).save();
        return articleExist;
    }

    @Cron('* */60 * * * *')
    async getArticlesApi(): Promise<void>{
        this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .subscribe(async ({data}) =>{
            const articles: ArticleInterface[] = data['hits']
            articles.forEach( async el =>{
                const artilceDeletedFind = 
                await this.articleDeletedModel.find({objectId: el.objectID});
                const articleExist = 
                await this.articleModel.find({objectId: el.objectID});
                if(artilceDeletedFind.length === 0 && articleExist.length === 0){                
                    const objCreate: CreateArticleDto = {
                        author: el.author,
                        objectId: el.objectID,
                        createdAt: el.created_at,
                        title: el.title,
                        storyTitle: el.story_title,
                        url: el.url,
                        storyUrl: el.story_url,
                    }
                    const obj = new this.articleModel(objCreate);
                    await obj.save();
                }
                
            })
            return true

        }, err =>{
            console.log(err)
            return false
        })
    
    }
}
